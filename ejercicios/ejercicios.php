<?php
	require 'conexion.php';
?>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-theme.css" rel="stylesheet">
		<link href="css/jquery.dataTables.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" href="http://132.248.203.250/~joan/css/main.css">
	</head>
	<body>
        <!-- NAVIGATION -->
     <nav id="menu" class="navbar navbar-expand-lg">
      <div class="container">
        <a class="navbar-brand" href="home.php">
          <img src="http://132.248.203.250/~joan/img/log0.jpg"  class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/home.php"><button type="button" class="btn btn-dark">Inicio</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/nosotros.php"><button type="button" class="btn btn-dark">Nosotros</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/conocenos.php">
              <button type="button" class="btn btn-dark">Conócenos</button></a>
            </li>  <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/ejercicios.php"><button type="button" class="btn btn-dark">Ejercicios</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/contacto.php">
            <button type="button" class="btn btn-dark">Contacto</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/editar.php">
            <button type="button" class="btn btn-dark">Editar</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/index.php">
              <button type="button" class="btn btn-dark">Cerrar sesión</button>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
	<div class="container">
		<div class="row" style="padding: 10px">
			<h2>Lista de ejercicios</h2>
		</div>
		<br>
		<div class="table-responsive-sm">
			<table id="mitabla" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
				<thead>
					<tr class="alert-success">
						<th>ID</th>
						<th>Nombre</th>
						<th>Dificultad</th>
						<th>ver</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="confirm-delete" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">Eliminar Registro</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					¿Desea eliminar este registro?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a class="btn btn-danger btn-ok">Eliminar</a>
				</div>
			</div>
		</div>
	</div>
  <!-- Mapa -->
    <section id="mapa">
      <h3 class="section-title" align="center">  <a class="navbar-brand" href="#foot">
        <img src="http://132.248.203.250/~joan/img/log0.jpg"  class="logo">
      </a></h3>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1881.5430424530161!2d-99.02148694229211!3d19.40868616490738!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fcc934207971%3A0x1528192d53465b0b!2sLas%20Golondrinas%20117%2C%20Benito%20Ju%C3%A1rez%2C%2057000%20Nezahualc%C3%B3yotl%2C%20M%C3%A9x.!5e0!3m2!1ses!2smx!4v1620338215532!5m2!1ses!2smx" allowfullscreen></iframe>
      </div>
    </section>
    <footer id="foot">
      <div class="container p-3">
        <div class="row text-center text-white">
          <div class="col ml-auto">
            <p>Copyright ©Todos los Derechos Reservados 2021</p>
          </div>
        </div>
      </div>
    </footer>
	<script src="js/jquery-3.5.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
	<script src="js/fontawesome.js"></script>
	<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				responsive: true,
				"order": [[0, "asc"]],
				"language":{
					"lengthMenu": "Mostrar _MENU_ registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"processing":     "Procesando...",
					"search": "Buscar:",
					"zeroRecords":    "No se encontraron registros coincidentes",
					"paginate": {
						"next":       "Siguiente",
						"previous":   "Anterior"
					},
				},
				"bProcessing": true,
				"bServerSide": true,
				"sAjaxSource": "server_process.php"
			});
		});
	</script>
	<script>
		$('#confirm-delete').on('show.bs.modal', function(e) {
			$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
		});
	</script>
</body>
</html>