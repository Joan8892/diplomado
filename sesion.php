<?php
function salir($exit){
	//Termino de la sesion
	if(isset($exit) && $exit == true){
		session_destroy();
		header('Location:login.php');
		exit();
	}
}