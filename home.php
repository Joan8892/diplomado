<?php
session_start();
include 'database.php';
//Termina la sesion
if(isset($_GET['logout'])){
salir($_GET['logout']);
}
//No permite visualizar contenido sin sesion
if (!isset($_SESSION['name']) && empty($_SESSION['name'])) {
header('Location:login.php');
}
#Conexion
$dbc = conectar();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HB</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- CUSTOM CSS -->
    <link href="http://132.248.203.250/~joan/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="http://132.248.203.250/~joan/css/main.css">
    <link rel="stylesheet" href="http://132.248.203.250/~joan/css/flotantes.css">
    <link href="http://132.248.203.250/~joan/css/style.css" rel="stylesheet">
    <link rel="Shortcut Icon" href="http://132.248.203.250/~joan/img/favicon-32x32.png" type="image/x-icon"/>
  </head>
  <body>
    <!-- Section: home video -->
    <section id="intro" class="home-video text-light">
      <div class="home-video-wrapper">
        <div class="homevideo-container">
          <div id="P1" class="bg-player" style="display:block; margin: auto; background: rgba(0,0,0,0.5)" data-property="{videoURL:'https://www.youtube.com/watch?v=r9o2GnJE1nc',containment:'.homevideo-container', quality:'hd720', showControls: false, autoPlay:true, mute:false, startAt:0, opacity:1}"></div>
        </div>
        <div class="overlay">
          <div class="text-center video-caption">
            <div class="wow bounceInUp" data-wow-offset="0" data-wow-delay="1s">
              <div class="margintop-30">
                <a href="#menu" class="btn btn-skin"id="btn-scroll">Iniciar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /Section: intro -->
    <!-- NAVIGATION -->
    <nav id="menu" class="navbar navbar-expand-lg">
      <div class="container">
        <a class="navbar-brand" href="home.php">
          <img src="http://132.248.203.250/~joan/img/log0.jpg"  class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/home.php"><button type="button" class="btn btn-dark">Inicio</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/nosotros.php"><button type="button" class="btn btn-dark">Nosotros</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/conocenos.php">
              <button type="button" class="btn btn-dark">Conócenos</button></a>
            </li>  <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/ejercicios.php"><button type="button" class="btn btn-dark">Ejercicios</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/contacto.php">
            <button type="button" class="btn btn-dark">Contacto</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/editar.php">
            <button type="button" class="btn btn-dark">Editar</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/index.php">
              <button type="button" class="btn btn-dark">Cerrar sesión</button>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- HEADER -->
  <div class="flotantes">
    <div id="flotante1">
       <a href="https://twitter.com/"> <img src="img/tw1.png" class="rounded-pill">
    </div> 
      
      <div id="flotante2">
        <a href="https://www.facebook.com/"><img src="img/fb.png" class="rounded-pill">
    </div>

    <div id="flotante3">
        <img src="img/yb1.png" class="rounded-pill"> <a href="https://www.youtube.com/">
    </div>

    <div id="flotante4">
        <img src="img/ins2.png" class="rounded-pill"><a href="https://www.instagram.com/">
    </div>
</div>
  <!-- HEADER -->
  <header class="main-header">
    <div class="background-overlay text-white py-5">
      <div class="container">
        <div class="row d-flex h-100">
          <div class="col-sm-6 text-center justify-content-center align-self-center">
            <a class="navbar-brand" href="#info">
              <img src="img/log0.jpg"  class="logo">
            </a>
            <h1>BrotherHood</h1>
            <h1>¡Forma parte de nuestra Hermandad!</h1>
            <h3>Y a ti que ya perteneces a ella... Gracias</h3>
            <h4>"Nadie es más fuerte que nosotros juntos"</h4>
            
            <h4>We are fucking awesome</h4>
            <h4>We are BrotherHood crossfit</h4>
          </div>
          <div id="producto" class="col-sm-6">
            <img src="img/producc.png" class="img-fluid d-none d-sm-flex">
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- FEATURES -->
  <section id="info" class="py-3">
    <div class="d-flex flex-row justify-content-center">
      <a href="#team" class="btn  btn-lg  align-middle">
        <img src="img/log0.jpg"  class="logo">
      </a>
    </div>
    <h2 style=" background: #46f024">Conócenos!!!</h2>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <div class="card text-white " style="background-color:#46f024 ">
            <div class="card-body">
              <h3>Te invitamos a formar parte de esta gran Hermandad </h3>
              <img src="img/principal.jpg" class="img-fluid" alt="Promo">
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn" style="background-color: #fff" data-toggle="modal" data-target="#image1">
            Info
            </button>
            <!-- Modal -->
            <div class="modal fade" id="image1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle" style="color: black">¡Contamos con 8 años de recorrido!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a class="portfolio-item">
                    <img src="img/principal.jpg" class="img-fluid" alt="Promo"> </a>
                  </div>
                  <div class="modal-footer">
                    <h5 style="color: black">
                    We are fucking awesome. We are BrotherHood crossfit
                    </h5>
                    <button type="button" class="btn " style="background-color: #46f024" data-dismiss="modal"><h5 style="color: white">Cerrar</h5></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card text ">
            <div class="card-body">
              <h3>¡Ven conocenos y agenda tus primeras 2 clases gratis! </h3>
              <img src="img/agenda.jpg" class="img-fluid" alt="Agenda">
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn" style="background-color: #46f024" data-toggle="modal" data-target="#image2">
            Info
            </button>
            <!-- Modal -->
            <div class="modal fade" id="image2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">¡Ven a conocer nuestras instalaciones, entrenamiento y a nuestro equipo de Coaches!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a class="portfolio-item">
                    <img src="img/agenda.jpg" class="img-fluid" alt="Promo"> </a>
                  </div>
                  <div class="modal-footer">
                    <p>
                      Tus primeras dos clases ¡NO PAGAS!<br>
                      (Sólo nuevo ingreso)
                    </p>
                    <button type="button" class="btn " style="background-color: #46f024" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card text-white " style="background-color:#46f024 ">
            <div class="card-body">
              <h3>¡La edad no es ninguna excusa para no ir al box! </h3>
              <img src="img/categorias.jpg" class="img-fluid" alt="Categorias">
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn" style="background-color: #fff" data-toggle="modal" data-target="#image3">
            Info
            </button>
            <!-- Modal -->
            <div class="modal fade" id="image3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle" style="color: black">El Crossfit es como el amor, no tiene edad ¿O sí?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a class="portfolio-item1">
                    <img src="img/categorias.jpg" class="img-fluid" alt="Categorias"> </a>
                  </div>
                  <div class="modal-footer">
                    <h5 style="color: black">
                    Aprovecha inscripcción al 50%
                    </h5>
                    <button type="button" class="btn " style="background-color: #46f024" data-dismiss="modal"><h5 style="color: white">Cerrar</h5></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 p-4">
          <div class="card text ">
            <div class="card-body">
              <h3>Existen dos tipos de enfoque hacia la práctica del CrossFit</h3>
              <img src="img/recreativo.jpg" class="img-fluid" alt="Promo">
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn" style="background-color: #46f024" data-toggle="modal" data-target="#image4">
            Info
            </button>
            <!-- Modal -->
            <div class="modal fade" id="image4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">recreativo en donde las personas entran a su clase regular con un Workout</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a class="portfolio-item">
                    <img src="img/recreativo.jpg" class="img-fluid" alt="Recreativo"> </a>
                  </div>
                  <div class="modal-footer">
                    <p>
                      Es tal ves el más emocionante, espectacular pero también desgastante y que involucra una preparación previa para poder desempeñarse adecuadamente.
                    </p>
                    <button type="button" class="btn " style="background-color: #46f024" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 p-4">
          <div class="card text-white " style="background-color:#46f024 ">
            <div class="card-body">
              <h3>¡Ven conocenos y agenda tus primeras 2 clases gratis! </h3>
              <img src="img/extras.jpg" class="img-fluid" alt="Extras">
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn" style="background-color: #fff" data-toggle="modal" data-target="#image5">
            Info
            </button>
            <!-- Modal -->
            <div class="modal fade" id="image5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle" style="color: black">¡Ven a conocer nuestras instalaciones, entrenamiento y a nuestro equipo de Coaches!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a class="portfolio-item">
                    <img src="img/extras.jpg" class="img-fluid" alt="Extras"> </a>
                  </div>
                  <div class="modal-footer">
                    <h5 style="color: black">
                    Tus primeras dos clases ¡NO PAGAS!<br>
                    (Sólo nuevo ingreso)
                    </h5>
                    <button type="button" class="btn " style="background-color: #46f024" data-dismiss="modal"><h5 style="color: white">Cerrar</h5></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 p-4">
          <div class="card text ">
            <div class="card-body">
              <h3>Medidas sanitarias </h3>
              <img src="img/medidas.jpg" class="img-fluid" alt="Sanitarias">
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn" style="background-color: #46f024" data-toggle="modal" data-target="#image6">
            Info
            </button>
            <!-- Modal -->
            <div class="modal fade" id="image6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Medidas sanitarias dentro del box</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a class="portfolio-item">
                    <img src="img/medidas.jpg" class="img-fluid" alt="Sanitarias"> </a>
                  </div>
                  <div class="modal-footer">
                    <p>
                      Debido a la situación actual es indispensable cumplir las medidas sanitarias
                    </p>
                    <button type="button" class="btn " style="background-color: #46f024" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /Section: testimonial -->
  <!-- TEAM -->
  <section id="team" class="text-center team">
    <div id="equipo" class="container">
      <a class="navbar-brand" href="#mapa">
        <img src="img/log0.jpg"  class="logo">
      </a>
      <h2 class="text text-white">Coach</h2>
      <div class="row">
        <section id="#coach">
          <div class="bd-example" id="testimonial" class="home-section paddingbot-60 parallax" data-stellar-background-ratio="0.5">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="5"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item active py-3">
                  <div class="col-12 text-center">
                    <div class="block-text rel zmin">
                      <a title="Entrenadores" href="#c1">TEAM</a>
                      <p>El Crossfit es una disciplina deportiva que, como cualquier otra, requiere de un profesor especializado y altamente capacitado para enseñar a practicarlo de la manera correcta. En BrotherHood estamos comprometidos en predicar con el ejemplo, esforzándonos a crecer en nuestro propio entrenamiento para motivarte a mejorar día con día la técnica y darlo todo en cada WOD.</p>
                      <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                    </div>
                    <div class="person-text rel text-light">
                      <img id="c1" src="img/coach.jpeg" alt="" class="rounded-circle" alt="Entrenadores" width="304" height="350" />
                      <a title="Coach" href="#mapa">Edo. México</a>
                      <div class="d-flex flex-row justify-content-center">
                        <div class="p-4">
                          <a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                        </div>
                        <div class="p-4">
                          <a href="https://web.whatsapp.com/"><i class="fab fa-whatsapp"></i></a>
                        </div>
                        <div class="p-4">
                          <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="carousel-item py-3">
                  <div class="col-12 text-center">
                    <div class="block-text rel zmin">
                      <a title="Cesar JR" href="c2">Cesar Carmona Jr</a>
                      <p>Siempre buscando el siguiente desafío amante del crossfit y transmitir la pasión por esta disciplina ayudando a las personas que quieran superarse todos los días. Hay que machacar las bases y repetir muchísimas veces cada movimiento. </p>
                      <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                    </div>
                    <div class="person-text rel text-light">
                      <img id="c2" src="img/ce.jpg" alt="coach" class="rounded-circle" alt="Cesar JR" width="304" height="350" />
                      <a title="" href="#mapa">Edo. México</a>
                      <div class="d-flex flex-row justify-content-center">
                        <div class="p-4">
                          <a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                        </div>
                        <div class="p-4">
                          <a href="https://web.whatsapp.com/"><i class="fab fa-whatsapp"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="carousel-item py-3">
                  <div class="col-12 text-center">
                    <div class="block-text rel zmin">
                      <a title="Betsy" href="#c3">Betsy</a>
                      <p>Siempre ha estado vinculada al mundo del ejercicio en una u otra forma, pero Brotherhood me ayudó a desarrollar y confiar en mis capacidades fijandome como meta ser una de las mejores atletas RX. Como coach me gusta aportar una gran energía en todas las clases e inspirar a mis alumnos a cumplir todas sus metas entrenando con disciplina.</p>
                      <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                    </div>
                    <div class="person-text rel text-light">
                      <img id="c3" src="img/an.jpg" alt="" class="rounded-circle" alt="Betsy" width="304" height="350" />
                      <a title="" href="#mapa">Edo. México</a>
                      <div class="d-flex flex-row justify-content-center">
                        <div class="p-4">
                          <a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="carousel-item py-3">
                  <div class="col-12 text-center">
                    <div class="block-text rel zmin">
                      <a title="cesar" href="#c4">Cesar</a>
                      <p>Mi pasión por el deporte me llevó a descubrir el mundo del crossfit hace 6 años, me saco de la rutina diaria y me impulso a lograr cosas que jamás pensé realizar a mi edad. Como coach me gustas inspirar a todas las personas mayores a darse cuenta que la edad solo es un número y no tenemos límites.</p>
                      <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                    </div>
                    <div class="person-text rel text-light">
                      <img id="c4" src="img/coach.jpg" alt="" class="rounded-circle" alt="cesar" width="304" height="350" />
                      <a title="" href="#mapa">Edo. México</a>
                      <div class="d-flex flex-row justify-content-center">
                        <div class="p-4">
                          <a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                        </div>
                        <div class="p-4">
                          <a href="https://web.whatsapp.com/"><i class="fab fa-whatsapp"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="carousel-item py-3">
                  <div class="col-12 text-center">
                    <div class="block-text rel zmin">
                      <a title="Eri" href="#c5">Erika</a>
                      <p>Mi meta es llegar a representar a México en los Crossfit Games y como coach compartir lo que he aprendido día a día con los alumnos. BrotherHood no solo es un lugar para entrenar, es un lugar que te integra a una gran comunidad que te motiva a ser la mejor versión de ti. </p>
                      <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                    </div>
                    <div class="person-text rel text-light">
                      <img id="c5" src="img/lor.jpg" alt="" class="rounded-circle" alt="Eri" width="304" height="350" />
                      <a title="" href="#mapa">Edo. México</a>
                      <div class="d-flex flex-row justify-content-center">
                        <div class="p-4">
                          <a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                        </div>
                        <div class="p-4">
                          <a href="https://web.whatsapp.com/"><i class="fab fa-whatsapp"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="carousel-item py-3">
                  <div class="col-12 text-center">
                    <div class="block-text rel zmin">
                      <a title="Wald" href="#c6">Waldo</a>
                      <p>BrotherHood es la oportunidad que me dan cada día para ser mejor entrenador, paralelamente disfrutar la energía que emana toda la hermandad de esta comunidad, mi meta como coach es la consecución integral y espiritual de todos mis atletas. </p>
                      <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                    </div>
                    <div class="person-text rel text-light">
                      <img id="c6" src="img/wald.jpg" alt="" class="rounded-circle" alt="Wald" width="304" height="350" />
                      <a title="" href="#mapa">Edo. México</a>
                      <div class="d-flex flex-row justify-content-center">
                        <div class="p-4">
                          <a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                        </div>
                        <div class="p-4">
                          <a href="https://web.whatsapp.com/"><i class="fab fa-whatsapp"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
   
    <!-- Mapa -->
    <section id="mapa">
      <h3 class="section-title" align="center">  <a class="navbar-brand" href="#foot">
        <img src="img/log0.jpg"  class="logo">
      </a></h3>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1881.5430424530161!2d-99.02148694229211!3d19.40868616490738!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fcc934207971%3A0x1528192d53465b0b!2sLas%20Golondrinas%20117%2C%20Benito%20Ju%C3%A1rez%2C%2057000%20Nezahualc%C3%B3yotl%2C%20M%C3%A9x.!5e0!3m2!1ses!2smx!4v1620338215532!5m2!1ses!2smx" allowfullscreen></iframe>
      </div>
    </section>
    <footer id="foot">
      <div class="container p-3">
        <div class="row text-center text-white">
          <div class="col ml-auto">
            <p>Copyright ©Todos los Derechos Reservados 2021</p>
          </div>
        </div>
      </div>
    </footer>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <!-- Core JavaScript Files -->
    <script src="http://132.248.203.250/~joan/js/slippry.min.js"></script>
    <script src="http://132.248.203.250/~joan/js/jquery.flexslider-min.js"></script>
    <script src="http://132.248.203.250/~joan/js/jquery.mb.YTPlayer.js"></script>
    <script src="http://132.248.203.250/~joan/js/jquery.appear.js"></script>
    <script src="http://132.248.203.250/~joan/js/wow.min.js"></script>
    <script src="http://132.248.203.250/~joan/js/owl.carousel.min.js"></script>
    <script src="http://132.248.203.250/~joan/js/nivo-lightbox.min.js"></script>
    <script src="http://132.248.203.250/~joan/js/custom.js"></script>
  </body>
</html>