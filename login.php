<?php

  session_start();

include 'database.php';

$nombre      = isset($_POST['name']) ? filter_var(filter_var(ucwords(trim($_POST['name'])), FILTER_SANITIZE_SPECIAL_CHARS), FILTER_SANITIZE_STRING) : '';
$correo      = isset($_POST['email']) ? filter_var(trim($_POST['email']), FILTER_SANITIZE_SPECIAL_CHARS) : '';
$contrasenia = isset($_POST['password']) ? filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING) : '';

if (isset($_POST['submit'])) {

    if (!preg_match('/^[a-z\s]+$/i', $nombre)) {
        $error['name'] = 'Proporciona un nombre valido';
    }
    if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
        $error['email'] = 'Proporciona un correo valido';
    }

    if (strlen($contrasenia) < 8 || strlen($contrasenia) > 16) {
        $error['password'] = 'Tu contraseña es de 8 a 16 caracteres';
    }

    if (empty($error['name']) && empty($error['email']) && empty($error['password'])) {

        $dbc = conectar();

        $query = 'SELECT * FROM user WHERE name = ? AND email = ?';

        $stmt = mysqli_prepare($dbc, $query);

        mysqli_stmt_bind_param($stmt, 'ss', $nombre, $correo);

        mysqli_stmt_execute($stmt);

        $result = mysqli_stmt_get_result($stmt);

        $numRows = mysqli_affected_rows($dbc);

        if ($result && $numRows) {
            $user = mysqli_fetch_all($result, MYSQLI_ASSOC);
            if (password_verify($contrasenia, $user[0]["password"])) {
                $_SESSION['name'] = $user[0]["name"];
                header('Location:home.php');
            } else {
                $error['login'] = 'Vuelve a intentarlo';
            }
        } else {
            $error['login'] = 'Vuelve a intentarlo';
        }
    }
}
?>


    <?php include 'header.php' ?>

       <main class="container d-flex align-content-center justify-content-center py-5 ">
              <form action="<?php $_SERVER['PHP_SELF'];?>" method="post">
              <h2>Iniciar Sesión</h2>
      
          <input type="text" class="form-control" id="name" aria-describedby="Name" placeholder="Nombre" name="name" value="<?=$nombre;?>">
          <label for="name">Introduce Nombre</label>
     
          <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email" name="email" value="<?=$correo;?>">
          <label for="email">Introduce Email</label>
     
          <input type="password" class="form-control" id="password" placeholder="Password" name="password" value="<?=$contrasenia;?>">
          <label for="password">Introduce Password</label>
          <?php if (isset($error)) {?>
          <?php foreach ($error as $error) {?>
          <div class="alert alert-danger" role="alert">
            <span><?=$error;?></span>
          </div>
          <?php }?>
        <?php }?>
        <div><span> <a href="signup.php">Registrate</a></span></div>
        <div>
        <button type="submit" class="btn btn-primary" name="submit">Entrar</button>
        </div>

    </form>
</main>
<?php include 'footer.php';?>
