<?php
session_start();
include 'database.php';
//Termina la sesion
if(isset($_GET['logout'])){
salir($_GET['logout']);
}
//No permite visualizar contenido sin sesion
if (!isset($_SESSION['name'])) {
header('Location:login.php');
}
#Conexion
$dbc = conectar();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proyecto</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/instalaciones.css">
  </head>
  <body>
        <!-- NAVIGATION -->
    <nav id="menu" class="navbar navbar-expand-lg">
      <div class="container">
        <a class="navbar-brand" href="home.php">
          <img src="http://132.248.203.250/~joan/img/log0.jpg"  class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/home.php"><button type="button" class="btn btn-dark">Inicio</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/nosotros.php"><button type="button" class="btn btn-dark">Nosotros</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/conocenos.php">
              <button type="button" class="btn btn-dark">Conócenos</button></a>
            </li>  <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/ejercicios.php"><button type="button" class="btn btn-dark">Ejercicios</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/contacto.php">
            <button type="button" class="btn btn-dark">Contacto</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/editar.php">
            <button type="button" class="btn btn-dark">Editar</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/index.php">
              <button type="button" class="btn btn-dark">Cerrar sesión</button>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Page Content -->
  <section>
  <div class="container">
    <h2 class="font-weight-light text-center text-lg-left mt-4 mb-0">Sobre nosotros</h2>
    <hr class="mt-2 mb-5">
    <div class="row text-center text-lg-left">
      <?php
      include("data.php");
      foreach ($info as $key=>$value){
      ?>
      <!-- Grid column -->
      <div class="col-lg-4 col-md-6 mb-4">
        <a href="#" data-toggle="modal" data-target="#modal4" data-url="<?=$value['url_video']?>" >
          <img class="img-fluid z-depth-1" src="<?=$value['url_imagen']?>" alt="video">
        </a>
      </div>
      <!-- Grid column -->
      <?php
      }
      ?>
      <!--Modal: Name-->
      <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <!--Content-->
          <div class="modal-content">
            <!--Body-->
            <div class="modal-body mb-0 p-0">
              <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                <iframe class="embed-responsive-item" src=""
                allowfullscreen></iframe>
              </div>
            </div>
            </section>
     <!-- Mapa -->
    <section id="mapa">
      <h3 class="section-title" align="center">  <a class="navbar-brand" href="#foot">
        <img src="img/log0.jpg"  class="logo">
      </a></h3>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1881.5430424530161!2d-99.02148694229211!3d19.40868616490738!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fcc934207971%3A0x1528192d53465b0b!2sLas%20Golondrinas%20117%2C%20Benito%20Ju%C3%A1rez%2C%2057000%20Nezahualc%C3%B3yotl%2C%20M%C3%A9x.!5e0!3m2!1ses!2smx!4v1620338215532!5m2!1ses!2smx" allowfullscreen></iframe>
      </div>
    </section>
    <footer id="foot">
      <div class="container p-3">
        <div class="row text-center text-white">
          <div class="col ml-auto">
            <p>Copyright ©Todos los Derechos Reservados 2021</p>
          </div>
        </div>
      </div>
    </footer>
            <!-- BOOTSTRAP SCRIPTS -->
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
            <script src="script.js"></script>
          </body>
        </html>