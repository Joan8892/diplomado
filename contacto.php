<?php
  session_start();

  include 'database.php';

  //Termina la sesion
  if(isset($_GET['logout'])){
    salir($_GET['logout']);
  }

  //No permite visualizar contenido sin sesion
  if (!isset($_SESSION['name'])) {
    header('Location:login.php');
  }
  #Conexion
  $dbc = conectar();
  ?><script src="js/jquery.dataTables.min.js"></script>
    <script src="js/fontawesome.js"></script>
  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proyecto</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/contacto.css">
  </head>
  <body>
       <!-- NAVIGATION -->
    <nav id="menu" class="navbar navbar-expand-lg">
      <div class="container">
        <a class="navbar-brand" href="home.php">
          <img src="http://132.248.203.250/~joan/img/log0.jpg"  class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/home.php"><button type="button" class="btn btn-dark">Inicio</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/nosotros.php"><button type="button" class="btn btn-dark">Nosotros</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/conocenos.php">
              <button type="button" class="btn btn-dark">Conócenos</button></a>
            </li>  <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/ejercicios.php"><button type="button" class="btn btn-dark">Ejercicios</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/contacto.php">
            <button type="button" class="btn btn-dark">Contacto</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/editar.php">
            <button type="button" class="btn btn-dark">Editar</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/index.php">
              <button type="button" class="btn btn-dark">Cerrar sesión</button>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!--==========================
  Contact Section
  ============================-->
  <section id="contact">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h2 class="section-title" align="center" style="background: #46f024">¿Cómo contactarnos?</h2>
          <div class="section-title-divider">No esperes más tiempo y envíanos un mensaje a través de nuestras redes sociales Facebook, WhatsApp o también por correo electrónico</div>
          <p class="section-description">Te proporcionaremos ayuda rápida y amigable a través de nuestros medios de contacto de 06:00 a.m. a 21:00 p.m. todos los días de la semana</p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-3 col-md-push-2 p-3">
          <div class="info">
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Estado de México, Nezahualcóyotl<br>Golondrinas #117 C.P.57000</p>
            </div>
            <div>
              <i class="fa fa-envelope"></i>
              <p>BrotherHood@hotmail.com</p>
            </div>

            <div>
              <i class="fa fa-phone"></i>
              <p>55 3939 5880<br>55 3211 0645</p>

            </div>

          </div>
        </div>

        <div class="col-md-5 col-md-push-2 p-3">
          <div class="form">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <form action="contactform/mandar.php" method="post" role="form" class="contactForm">
              <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Por favor ingrese al menos 4 caracteres" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Ingresa un Correo Valido" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Por favor ingrese al menos 8 caracteres del tema" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Por favor escribe algo para nosotros" placeholder="Message"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center"><button type="submit">Enviar mensaje</button></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- Mapa -->
    <section id="mapa">
      <h3 class="section-title" align="center">  <a class="navbar-brand" href="#foot">
        <img src="img/log0.jpg"  class="logo">
      </a></h3>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1881.5430424530161!2d-99.02148694229211!3d19.40868616490738!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fcc934207971%3A0x1528192d53465b0b!2sLas%20Golondrinas%20117%2C%20Benito%20Ju%C3%A1rez%2C%2057000%20Nezahualc%C3%B3yotl%2C%20M%C3%A9x.!5e0!3m2!1ses!2smx!4v1620338215532!5m2!1ses!2smx" allowfullscreen></iframe>
      </div>
    </section>
    <footer id="foot">
      <div class="container p-3">
        <div class="row text-center text-white">
          <div class="col ml-auto">
            <p>Copyright ©Todos los Derechos Reservados 2021</p>
          </div>
        </div>
      </div>
    </footer>
     <!-- BOOTSTRAP SCRIPTS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
     <script src="contactform/contactform.js"></script>
  </body>
</html>