<?php

  include 'database.php';
#Envios POST
$name            = isset($_POST['name']) ? filter_var(filter_var(ucwords(trim($_POST['name'])), FILTER_SANITIZE_SPECIAL_CHARS), FILTER_SANITIZE_STRING) : '';
$email           = isset($_POST['email']) ? filter_var(trim($_POST['email']), FILTER_SANITIZE_SPECIAL_CHARS) : '';
$password        = isset($_POST['password']) ? filter_var(trim($_POST['password']), FILTER_SANITIZE_SPECIAL_CHARS) : '';
$verify_password = isset($_POST['verify_password']) ? filter_var(trim($_POST['verify_password']), FILTER_SANITIZE_SPECIAL_CHARS) : '';

$success['registro'] = '';

if (isset($_POST['submit'])) {

    if (!preg_match('/^[a-z\s]+$/i', $name)) {
        $error['nombre'] = 'Proporciona un nombre valido';
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error['email'] = 'Proporciona un correo valido';
    }

    if (strlen($password) < 8 || strlen($password) > 16) {
        $error['password']['longitud'] = 'Tu contraseña debe ser de 8 a 16 caracteres';
    }

    if ($password !== $verify_password) {
        $error['password']['verificacion'] = 'La contraseña no coincide';
    }

    #Conexion a la BD
    $dbc = conectar();

    #Ningun error
    if ($dbc && empty($error['nombre']) && empty($error['email']) && empty($error['password'])) {

      $query = 'SELECT * FROM user WHERE name = ? AND email = ?';

        $stmt = mysqli_prepare($dbc, $query);

        mysqli_stmt_bind_param($stmt, 'ss' ,$name, $email);

        mysqli_stmt_execute($stmt);

        $result = mysqli_stmt_get_result($stmt);

        $numRows = mysqli_affected_rows($dbc);

        if (!$numRows) {
          $query = "INSERT INTO user(name, email, password, status) VALUES (?,?,?,?)";

          $stmt = mysqli_prepare($dbc, $query);

          $status = 'enabled';

          $password_hash = password_hash($password, PASSWORD_DEFAULT);

          mysqli_stmt_bind_param($stmt, 'ssss', $name, $email, $password_hash, $status);

          if (!mysqli_stmt_execute($stmt)) {
              $error['registro'] = "No se pudo insertar el registro";
          } else {
              $success['registro'] = "Te haz registrado";
          }
      }else{
        $error['registro'] = "Nombre o correo no estan disponible";
      }
    }
}
?>
<?php include 'header.php';?>
<main class="container d-flex align-content-center justify-content-center py-5">
  <form action="<?php $_SERVER['PHP_SELF'];?>" method="post">
      <h2>Regístrate</h2>
    
      
      
          <input type="text" class="form-control" id="name" placeholder="Nombre" name="name" value="<?=$name;?>">
          <label for="name">Introduce Nombre</label>
        </div>
        <?php if (!empty($error['nombre'])) {?>
        <div class="alert alert-danger" role="alert">
          <span><?=$error['nombre']?></span>
        </div>
        <?php }?>

          <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="<?=$email;?>">
           <label for="email">Introduce email</label>
        </div>
        <?php if (!empty($error['email'])) {?>
          <div class="alert alert-danger" role="alert">
            <span><?=$error['email']?></span>
          </div>
        <?php }?>

          <input type="password" class="form-control" id="password" placeholder="Password" name="password" value="<?=$password;?>">
          <label for="password">Introduce password</label>
          
          <input type="password" class="form-control" id="v-password" placeholder="Repeat Password" name="verify_password" value="<?=$verify_password;?>">
          <label for="v-password">Repite password</label>
          <?php if (!empty($error['password'])) { ?>
          <?php foreach ($error['password'] as $error) {?>
          <div class="alert alert-danger" role="alert">
            <span><?=$error;?></span>
          </div>
          <?php }?>
        <?php } ?>
        <div>
          <a href="login.php">Ya tengo cuenta</a>
        </div>
        <?php if(!empty($error['registro'])){?>
        <div class="alert alert-danger" role="alert">
          <span><?=$error['registro'];?></span>
        </div>
        <?php }elseif(!empty($success['registro'])){ ?>
          <div class="alert alert-success" role="alert">
            <span><?=$success['registro'];?>
              <a href="login.php"> -> Inicia Sesion</a>
            </span>
          </div>
        <?php }?>
        <button type="submit" class="btn btn-primary" name="submit">Create</button>

    </form>
  </main>

 <?php include 'footer.php';?>
