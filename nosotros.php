<?php
session_start();
include 'database.php';
//Termina la sesion
if(isset($_GET['logout'])){
salir($_GET['logout']);
}
//No permite visualizar contenido sin sesion
if (!isset($_SESSION['name'])) {
header('Location:login.php');
}
#Conexion
$dbc = conectar();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Proyecto</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/nosotros.css">
  </head>
  <body>
    <!-- NAVIGATION -->
    <nav id="menu" class="navbar navbar-expand-lg">
      <div class="container">
        <a class="navbar-brand" href="home.php">
          <img src="http://132.248.203.250/~joan/img/log0.jpg"  class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/home.php"><button type="button" class="btn btn-dark">Inicio</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/nosotros.php"><button type="button" class="btn btn-dark">Nosotros</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://132.248.203.250/~joan/conocenos.php">
              <button type="button" class="btn btn-dark">Conócenos</button></a>
            </li>  <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/ejercicios.php"><button type="button" class="btn btn-dark">Ejercicios</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/contacto.php">
            <button type="button" class="btn btn-dark">Contacto</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/ejercicios/editar.php">
            <button type="button" class="btn btn-dark">Editar</button></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://132.248.203.250/~joan/index.php">
              <button type="button" class="btn btn-dark">Cerrar sesión</button>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- ABOUT -->
  <section class="m5 text-center bg-light">
    <div class="container">
      <div class="row">
        <div class="m-5">
          <h2 style="background: #46f024">¿Quiénes somos?</h2>
          <p>Brotherhood es un recinto en el que cultivamos el CrossFit, que es una disciplina que viene ganando adeptos con mucha rapidez a nivel mundial. En Brotherhood encontraras algo diferente a los demás box de CrossFit, ya que ofrecemos una formación competitiva para todas las edades, así como prácticas de halterofilia con profesionales en la materia, formando campeones desde los 9 años de edad.
          </p>
          <img src="img/quienes.jpg" class="img-fluid" alt="Brother" width="400" height="300">
        </div>
      </div>
    </div>
  </section>
  <!-- ACCORDION -->
  <section class="container text-center p-5" style="background: #46f024" >
    <div class="row">
      <div class="accordion" id="accordionExample" >
        <div class="card">
          <div class="card-header" id="headingOne">
            <h3 class="mb-0" style="background: #46f024">
            <button class="btn btn-light" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            ¿Qué es CrossFit?
            </button>
            </h3>
          </div>
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
              CrossFit es el deporte de moda. Se basa en ejercicios funcionales realizados a alta intensidad. No hay rutinas ni excusas, cada día tu entreno es distinto. Se adapta a todos los niveles y está dirigido a cualquier persona.<br>
              Entrena el cuerpo ejerciendo diferentes disciplinas al mismo tiempo, tales como la halterofilia, el atletismo, la gimnasia y sobre todo la condición metabólica. Este programa se compone de deporte de resistencia y de diferentes actividades físicas. Se basa en el trabajo de diferentes capacidades y habilidades: resistencia cardiovascular y respiratoria, resistencia muscular, fuerza, flexibilidad, potencia, velocidad, agilidad, psicomotricidad, equilibrio y precisión. Todas estas actividades intervienen enérgicamente para una puesta en forma eficaz.
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo">
            <h3 class="mb-0">
            <button class="btn btn-light collapsed" style="background: #46f024" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            ¿Por qué CrossFit tiene tanto éxito?
            </button>
            </h3>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body" style="background: #46f024">
              <h5>
              En CrossFit, la tasa de deserción es inferior a la de los centros de entrenamiento tradicionales, probablemente a causa del espíritu de camaradería que se respira en cada Box.
              Cabe agregar que la continua variación de los entrenamientos, los materiales inusuales que se utilizan (Kettlebells, anillas, Battle Rope, etc.) y el hecho de que se trate de un sistema competitivo (tanto contra los demás como contra uno mismo) hace que el tiempo de entrenamiento pase volando.
              Por otra parte, CrossFit garantiza drásticos cambios físicos en poco tiempo porque cada entrenamiento se lleva hasta la extenuación. Esto contrasta con el entrenamiento tradicional donde muchas personas tienden a entrenar por debajo de sus posibilidades, quizá porque no tienen la supervisión de un coach o la presión grupal.
              </h5>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingThree">
            <h3 class="mb-0" style="background: #46f024">
            <button class="btn btn-light collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            ¿Quién lo puede practicar?
            </button>
            </h3>
          </div>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
              <h5>Este deporte puede practicarlo cualquier persona esté o no en forma física. De hecho, el éxito del CrossFit reside en su facilidad para adaptarse a cualquier condición.
              Es un sistema de entrenamiento perfecto para todo el mundo, en su nivel más alto de competición encontramos gente exigente que quiere esforzarse al máximo. En un nivel más ligero se encuentra la mayoría de crossfiters en el mundo. Hombres y mujeres de todas las edades que buscan ponerse en forma, estar más ágiles, saludables y conseguir grandes resultados con su cuerpo que se notan enseguida a la vista y en su actividad diaria.
              CrossFit es entrenamiento funcional, constantemente variado, a alta intensidad. El Box es el gimnasio. Y el wod, el entrenamiento que se realiza cada día. Este deporte puede practicarlo cualquier persona esté o no en forma física. De hecho, el éxito del CrossFit reside en su facilidad para adaptarse a cualquier condición.</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- Mapa -->
    <section id="mapa">
      <h3 class="section-title" align="center">  <a class="navbar-brand" href="#foot">
        <img src="img/log0.jpg"  class="logo">
      </a></h3>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1881.5430424530161!2d-99.02148694229211!3d19.40868616490738!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fcc934207971%3A0x1528192d53465b0b!2sLas%20Golondrinas%20117%2C%20Benito%20Ju%C3%A1rez%2C%2057000%20Nezahualc%C3%B3yotl%2C%20M%C3%A9x.!5e0!3m2!1ses!2smx!4v1620338215532!5m2!1ses!2smx" allowfullscreen></iframe>
      </div>
    </section>
    <footer id="foot">
      <div class="container p-3">
        <div class="row text-center text-white">
          <div class="col ml-auto">
            <p>Copyright ©Todos los Derechos Reservados 2021</p>
          </div>
        </div>
      </div>
    </footer>
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="script.js"></script>
</body>
</html>